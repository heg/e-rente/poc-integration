<!-- Footer start -->
<footer>
    <div class="container">
            <div class="row">
                <div class="col-lg-12 text-right">
                    <ul>
                        <li>
                            <a href="#">Aide</a>
                        </li>
                        <li>
                            <a href="#">Feedback</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
    </div>
    </footer>

    <!-- Footer end -->
    
    <!-- Jquery -->
    <script src="js/vendor/jquery-3.5.1.min.js"></script>
    <!-- Slider Slick JS -->
    <script src="js/vendor/slick.min.js"></script>
    <!-- Main -->
    <script src="js/dibs.js"></script>
  </body>
</html>