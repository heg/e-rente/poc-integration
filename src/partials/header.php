<!doctype html>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Adobe font -->
    <link rel="stylesheet" href="https://use.typekit.net/fpb8dgo.css">

    <!-- Bootstrap CSS -->
    <!--<link rel="stylesheet" href="css/vendor/bootstrap-reboot.min.css">-->
    <link rel="stylesheet" type="text/css" href="css/vendor/bootstrap-grid.min.css">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" type="text/css" href="css/vendor/all.min.css"/>
    <!-- Main CSS -->
    <link rel="stylesheet" type="text/css" href="css/dibs.css"/>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="img/favicon.png" />

    <title>DIBS<?php if (!empty($title)) echo ' | '.$title; ?></title>
  </head>
  <body>

    <!-- Header start -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 main-nav">
                    <a href="/" target="_blank" class="logo order-0">
                        <h1>e-rentes.ch</h1>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <!-- Header end -->