<?php include 'partials/header.php' ?>


    <!-- Main content start -->
    <div class="main-content">
        <div class="container">

            <h2>Connexion à <span class="text-bold">e-rentes.ch</span></h2>

            <div class="row">
                <div class="content col-md-8">
                    <img src="img/SwissID_logo.png" width="30%">
                    <div class="description">Pour accéder à la plateforme DIBS, veuillez-vous connecter avec votre identifiant SwissID</div>
                    <form>
                        <div class="field-group identifiant">
                            <div class="field">
                                <label for="identifiant">Identifiant</label>
                                <input type="text" name="identifiant" id="identifiant" placeholder="Identifiant SwissID"/>
                            </div>
                            <div class="user-no-info"><a href="#" onclick="alert('Création SwissID');">> Vous n'avez pas d'identifiant SwissID?</a></div>
                        </div>
                        <div class="field-group password">
                            <div class="field">
                                <label for="password">Mot de passe</label>
                                <input type="password" name="password" id="password" placeholder="******"/>
                            </div>
                        </div>
                        <div class="user-no-info">Vous n'avez pas d'identifiant SwissID? <a href="#" onclick="alert('Création SwissID');">Cliquez ici pour en créer un.</a></div>
                    </form>
                </div>
                <div class="actions col-md-4">
                    <div class="boutons">
                        <button class="back">Revenir à l'étape précédente</button>
                        <button class="next validation" disabled>Se connecter avec SwissID</button>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <!-- Main content end -->

<?php include 'partials/footer.php' ?>